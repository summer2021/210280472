## rsync 基本术语

在 rsync 程序的语境中，我们主要关注三对术语和三个进程，三对术语是：

- client - server

  强调连接的发起者和接收者，server 未必需要是 daemon

- sender - receiver

  强调文件的发送者和接收者

- source - destination

  在镜像站场景下，这对术语**极易被混淆**，它们实际上是对应 sender - receiver，却常被误以为对应 client - server，在下面的叙述中，我将尽量避免使用这对术语。

需要强调的是，前两对术语可以任意组合，即：发起连接者（申请开始同步的进程）可以是实际文件的发送者，也可以是接收者。事实上，rsync的 client - server 的概念存活周期很短，当rsync 连接（管道或网络套接字）建立后，将使用sender - receiver 来代替 client - server。

receiver 进程负责接收 sender 端发送的数据，以及完成文件重组的工作。receiver 端还有一个核心进程——generator进程。rsync的整个传输过程由这3个进程完成，它们是高度流水线化的，generator进程的输出结果作为sender端的输入，sender端的输出结果作为recevier端的输入。即形成如下的流水线（如此可以降低每份文件传输的 latency）：

> **generator-->sender-->receiver**

sender 端的 generator 比较本地目录和从 receiver 发来的 file list，进行一系列比较，最后计算出一个文件的校验码集合，发送给 sender，而 sender 进程一收到 generator 的校验码集合会立即开始处理文件，每遇到一个匹配块都会立即将这部分相关数据发送给receiver 进程，然后立即处理下一个数据块。receiver 进程收到 sender 发送的数据后，开始重组工作，最终完成同步。

## rsync 基本功能

rsync 可以在源路径和目标路径间实现文件的增量传输，下主要解释增量传输。

假设待传输文件为 A，如果目标路径下没有文件 A，则 rsync 会直接传输文件A，如果目标路径下已存在文件 A，则发送端视情况决定是否要传输文件 A。详细的流程是，发送端会先发送文件 A 的元数据及 checksum 列表，rsync 接收端收到该信息后进行对比，默认使用 quick check 算法，如果两端文件的大小或 mtime 不同，则发送端会传输该文件，否则将忽略该文件。若 quick check 算法决定了要传输文件 A，它只传源文件 A 和目标文件 A 所不同的部分。

rsync的增量传输体现在两个方面：文件级的增量传输和数据块级别的增量传输。文件级别的增量传输是指若源主机上有，目标主机上没有，则直接传输文件；数据块级别的增量传输是指只传输两文件的差异部分。

于是，**rsync 的核心需求就是——在传输尽量少的情况下，完成文件的差异对比。**

## rsync 核心算法

假设同步源文件名为 fileSrc，同步目的文件叫 fileDst。

### 分块Checksum算法

首先，rsync 会把 fileDst 的文件平均切分成若干个小块，比如每块512个字节（最后一块会小于这个数），然后对每块计算两个checksum：

- [rolling checksum](https://en.wikipedia.org/wiki/Rolling_hash)，在本文语境中，也称弱 checksum，32 位的 checksum，其使用的是Mark Adler发明的 [adler-32](https://en.wikipedia.org/wiki/Adler-32) 算法
- md5 hash：128 位强 checksum

这是一种 fast-path and slow-path 的思想：由于哈希碰撞的概率不同，rsync 使用弱 checksum 区别不同（计算开销小），而使用强 checksum 来确认相同（计算开销大）。（checksum的具体公式可以参看[这篇文章](https://rsync.samba.org/tech_report/node3.html)）

### 传输算法

同步目标端会把 fileDst 的一个 checksum 列表传给同步源，这个列表里包括了三个东西，**rolling checksum(32bits)**，**md5 checksume(128bits)**，**文件块编号**。

同步源机器拿到了这个列表后，会对 fileSrc 做同样的 checksum，然后和 fileDst 的checksum做对比，从而知道哪些文件块改变了。

### checksum 查找算法

同步源端拿到 fileDst 的 checksum 数组后，会用 rolling checksum 做 hash，把这个数据存到一个 hash table 中，以便获得 O(1) 时间复杂度的查找性能。这个 hash table 是 16 bits 的，所以，hash table 占用大小是 $2^{16}$，对 rolling checksum 的 hash 会被散列到 0 到 $2^{16} – 1$ 中的某个整数值。

### 比对算法

这是最关键的算法，细节如下：

1. 取 fileSrc 的第一个文件块（我们假设的长度是 512），也就是从 fileSrc 的第 1 个字节到第512 个字节，取出来后做 rolling checksum 计算。计算好的值到 hash 表中查。
2. 如果查到了，说明发现在 fileDst中有潜在相同的文件块，于是就再比较md5的checksum，因为rolling checksume太弱了，可能发生碰撞。于是还要算md5的128bits的checksum，这样一来，我们就有 2^-(32+128) = 2^-160的概率发生碰撞，这太小了可以忽略。**如果rolling checksum和md5 checksum都相同，这说明在fileDst中有相同的块，我们需要记下这一块在fileDst下的文件编号**。
3. 如果fileSrc的rolling checksum 没有在hash table中找到，那就不用算md5 checksum了。表示这一块中有不同的信息。总之，只要rolling checksum 或 md5 checksum 其中有一个在fileDst的checksum hash表中找不到匹配项，那么就会触发算法对fileSrc的rolling动作。于是，**算法会往后 step 1 个字节，取fileSrc中字节2-513的文件块要做checksum，go to 1** 。
4. 这样，我们就可以找出 fileSrc 相邻两次匹配中的那些文本字符，这些就是我们要往同步目标端传的文件内容了。

### 最终效果

最终，sender 会得到类似下图的一个数据数组，图中有色块表示在目标端已匹配上，不用传输（其中显示了两块chunk #5，表示可能重复），而无色的地方就是需要传输的内容（注意：这些无色的块是不定长的），这样，同步源这端把这个数组（无色的就是实际内容，红色的就放一个标号）压缩传到 receiver，receiver 会根据这个表重新生成文件，最终同步完成。

![rsync algorithm result](rsync-analysis.assets/rsync-algorithm-result.jpg)

## sender 和 receiver 的模块流程图

根据以上分析，结合实际源码的函数名，画出如下流程图。

![rsync-workflow](rsync-analysis.assets/rsync-workflow.png)

## rsync-huai 优化分析

rsync-huai 修改了服务端代码，rsync 的服务端在收到客户端同步请求时，会首先读本地文件列表，而这会产生大量 I/O 开销。rsync-huai 通过将文件的元数据提取出来，存储在快盘中，大大降低了这份开销。

其设计较巧妙：将待处理目录中所有文件的内容改为文件的真实 size（这是因为如果直接赋空，原 size 就丢失了），其它元数据不变，即可在这个构造出的 `mirror attr` 目录中读取所有需要的数据。